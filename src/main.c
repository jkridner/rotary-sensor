/*
 * Copyright (c) 2020 Friedt Professional Engineering Services, Inc
 * Copyright (c) 2020-2022 Jason Kridner, BeagleBoard.org Foundation
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/kernel.h>
#include <zephyr/device.h>
#include <zephyr/drivers/gpio.h>
#include <zephyr/drivers/led.h>
#include <zephyr/drivers/sensor.h>
#include <zephyr/drivers/adc.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdint.h>
#include <string.h>
#include <zephyr/net/net_ip.h>
#include <zephyr/net/socket.h>
#include <zephyr/sys/util.h>
#include <zephyr/random/rand32.h>
#include <zephyr/devicetree.h>
#include <zephyr/devicetree/io-channels.h>
#include <errno.h>
#include <zephyr/linker/sections.h>
#include <zephyr/net/net_core.h>
#include <zephyr/net/net_mgmt.h>

#include <math.h>

#define LOG_LEVEL LOG_LEVEL_INF
#include <zephyr/logging/log.h>
LOG_MODULE_REGISTER(rotary);

#define MCAST_IP6ADDR "ff84::2"

#define BLINK_MS 500
#define ROTARY_MS 100
#define UPDATE_MS 3000

#define MAX_STR_LEN 256
static char outstr[MAX_STR_LEN];

#define MAX_STR_IN_LEN 16

enum api {
	SENSOR_API,
	LED_API,
	ROTARY_API,
};

enum edev {
	LIGHT,
	ACCEL,
	HUMIDITY,
	LEDS,
	ROTARY,
	NUM_DEVICES,
};

static const struct gpio_dt_spec button = GPIO_DT_SPEC_GET(DT_ALIAS(sw0), gpios);

struct lnk_led_work {
	struct k_work_delayable dwork;
} lnk_led_work;
static const struct gpio_dt_spec led = GPIO_DT_SPEC_GET(DT_ALIAS(led0), gpios);

struct rotary_work {
	uint32_t position;
	uint32_t state;
	struct k_work_delayable dwork;
} rotary_work;
static struct gpio_callback rotary_cb_data;
static const struct gpio_dt_spec enA = GPIO_DT_SPEC_GET(DT_NODELABEL(enc_a), gpios);
static const struct gpio_dt_spec enB = GPIO_DT_SPEC_GET(DT_NODELABEL(enc_b), gpios);

static struct gpio_callback button_callback_data;

struct sensor_work {
	struct k_work_delayable dwork;
} sensor_work;

static const char *device_labels[NUM_DEVICES] = {
	[LIGHT] = "LIGHT",
	[ACCEL] = "ACCEL",
	[HUMIDITY] = "HUMIDITY",
	[LEDS] = "LEDS",
	[ROTARY] = "ROTARY",
};

static const char *device_names[NUM_DEVICES] = {
	[LIGHT] = "OPT3001-LIGHT",
	[ACCEL] = "LIS2DE12-ACCEL",
	[HUMIDITY] = "HDC2010-HUMIDITY",
	[LEDS] = "led_ring",
	[ROTARY] = "ROTARY",
};

static const enum api apis[NUM_DEVICES] = {
	SENSOR_API, /* LIGHT */
	SENSOR_API, /* ACCEL */
	SENSOR_API, /* HUMIDITY */
	LED_API, /* RING-LEDS */
	ROTARY_API,
};

static struct device *devices[NUM_DEVICES];

static struct sockaddr_in6 addr;
static int fd = -1;

static uint32_t recv_position;

static void set_rotary_leds()
{
	int start = recv_position;
	int finish = rotary_work.position;

	LOG_DBG("set_rotary_leds: %d -> %d", start, finish);

	for (int i = 0; i < 16; i++) {
		if (i == recv_position)
			led_on(devices[LEDS], i);
		else if (i == rotary_work.position)
			led_on(devices[LEDS], i);
		else
			led_off(devices[LEDS], i);
	}
}

static int lshift_string_to_separator(char *data, char separator, int size)
{
        char *pdata = data;

        pdata = strchr(data, separator);
        if (!pdata) {
                memset(data, 0, size);
                return 1;
        } else {
                int shift = (pdata - data) + 1;

                for (int i = 0; i < size; i++) {
                        if (i + shift < size)
                                data[i] = data[i + shift];
                        else
                                data[i] = 0;
                }
        }

        return 0;
}

static void do_recv_position()
{
	int c, k;
	char data[MAX_STR_IN_LEN];
	struct sockaddr src_addr;
	socklen_t addrlen;
	int idx;
	char chan;
	int val1, val2;
	unsigned long v;
	char * end;

	if (fd >= 0) {

		LOG_DBG("read");
		c = zsock_recvfrom(fd, data, MAX_STR_IN_LEN, 0, &src_addr, &addrlen);
		if (c <= 0)
			return;

		data[c] = '\0';
		LOG_DBG("scan: %s", data);
		if (data[1] != 'L' || data[2] != ':')
	       		return;
		for (k = 3; k < c; k++) {
			if (data[k] == '.' || data[k] == ';') {
				data[k] = '\0';
				break;
			}
		}
		if (k == c)
			return;
		v = strtoul(&data[3], &end, 10);
		val1 = (int) v;

		LOG_DBG("process: L:%d\n", val1);
		recv_position = val1;
		set_rotary_leds();
	}
}


static void recv_position_thread() {
	LOG_INF("Listening for UDP messages\n");
	for(;;) {
		do_recv_position();
		k_sleep(K_MSEC(100));
	}
}
#define STACK_SIZE 1024
#define THREAD_PRIORITY K_PRIO_PREEMPT(8)
K_THREAD_DEFINE(recv_position_thread_id, STACK_SIZE,
		recv_position_thread, NULL, NULL, NULL,
		THREAD_PRIORITY, 0, -1);

#if 0
static void setup_telnet_ipv6(struct net_if *iface)
{
	char hr_addr[NET_IPV6_ADDR_LEN];
	struct in6_addr addr;

	if (net_addr_pton(AF_INET6, CONFIG_NET_CONFIG_MY_IPV6_ADDR, &addr)) {
		LOG_ERR("Invalid address: %s", CONFIG_NET_CONFIG_MY_IPV6_ADDR);
		return;
	}

	net_if_ipv6_addr_add(iface, &addr, NET_ADDR_MANUAL, 0);

	LOG_INF("IPv6 address: %s",
		net_addr_ntop(AF_INET6, &addr, hr_addr,
					 NET_IPV6_ADDR_LEN));

	if (net_addr_pton(AF_INET6, MCAST_IP6ADDR, &addr)) {
		LOG_ERR("Invalid address: %s", MCAST_IP6ADDR);
		return;
	}

	net_if_ipv6_maddr_add(iface, &addr);
}
#endif

static void lnk_led_work_handler(struct k_work *work)
{
	ARG_UNUSED(work);

	gpio_pin_toggle_dt(&led);

	k_work_schedule(&lnk_led_work.dwork, K_MSEC(BLINK_MS));
}

static void print_sensor_value(size_t idx, const char *chan,
			       struct sensor_value *val)
{
	LOG_INF("%s: %s%d,%d", device_labels[idx], chan, val->val1, val->val2);

	sprintf(outstr+strlen(outstr), "%d%c:", idx, chan[0]);
	sprintf(outstr+strlen(outstr), "%d", val->val1);
	if (val->val2 != 0) {
		sprintf(outstr+strlen(outstr), ".%02d;", abs(val->val2) / 10000);
	} else {
		sprintf(outstr+strlen(outstr), ";");
	}
}

static void send_sensor_value()
{
	if ((fd >= 0) && (strlen(outstr) > 0)) {
		zsock_sendto(fd, outstr, strlen(outstr), 0,
			(const struct sockaddr *) &addr,
			sizeof(addr));
	}

	outstr[0] = '\0';
}

static void sensor_work_handler(struct k_work *work)
{
	struct sensor_value val;

	outstr[0] = '\0';

	for (size_t i = 0; i < NUM_DEVICES; ++i) {
		if (apis[i] == ROTARY_API) {
			val.val1 = rotary_work.position;
			val.val2 = 0;
			print_sensor_value(i, "r: ", &val);
			send_sensor_value();
			continue;
		}

		if (apis[i] != SENSOR_API) {
			continue;
		}

		if (devices[i] == NULL) {
			continue;
		}

		sensor_sample_fetch(devices[i]);

		if (i == LIGHT) {
			sensor_channel_get(devices[i], SENSOR_CHAN_LIGHT, &val);
			print_sensor_value(i, "l: ", &val);
			send_sensor_value();
			continue;
		}

		if (i == ACCEL) {
			sensor_channel_get(devices[i], SENSOR_CHAN_ACCEL_X,
					   &val);
			print_sensor_value(i, "x: ", &val);
			sensor_channel_get(devices[i], SENSOR_CHAN_ACCEL_Y,
					   &val);
			print_sensor_value(i, "y: ", &val);
			sensor_channel_get(devices[i], SENSOR_CHAN_ACCEL_Z,
					   &val);
			print_sensor_value(i, "z: ", &val);
			send_sensor_value();
			continue;
		}

		if (i == HUMIDITY) {
			sensor_channel_get(devices[i], SENSOR_CHAN_HUMIDITY,
					   &val);
			print_sensor_value(i, "h: ", &val);
			sensor_channel_get(devices[i], SENSOR_CHAN_AMBIENT_TEMP,
					   &val);
			print_sensor_value(i, "t: ", &val);
			send_sensor_value();
			continue;
		}
	}

	k_work_schedule(&sensor_work.dwork, K_MSEC(UPDATE_MS));
}

static void rotary_turned(const struct device *dev, struct gpio_callback *cb,
		uint32_t pins)
{
	int a = gpio_pin_get_dt(&enA);
	int b = gpio_pin_get_dt(&enB);

	if(a != b) {
		/* Encoder A changed first, then clockwise */
		rotary_work.position++;
		LOG_DBG("ROTARY position (CW): %d", rotary_work.position);
	} else {
		rotary_work.position--;
		LOG_DBG("ROTARY position (CCW): %d", rotary_work.position);
	}
	rotary_work.position %= 16;
	set_rotary_leds();

	k_work_reschedule(&sensor_work.dwork, K_MSEC(ROTARY_MS));
}

static void button_handler(const struct device *dev, struct gpio_callback *cb,
			   uint32_t pins)
{
	ARG_UNUSED(dev);

	/* BEL (7) triggers BEEP on MSP430 */
	LOG_INF("%c%s event", 7, "BUTTON");
	/* print sensor readings */
	k_work_reschedule(&sensor_work.dwork, K_NO_WAIT);
}

void main(void)
{
	int r;
	struct net_if *iface = net_if_get_default();

	outstr[0] = '\0';

	fd = socket(AF_INET6, SOCK_DGRAM, IPPROTO_UDP);
	__ASSERT(fd > 0, "socket() failed: %d", fd);
	memset(&addr, 0, sizeof(struct sockaddr_in6));
	addr.sin6_family = AF_INET6;
	addr.sin6_port = htons(9999);
	inet_pton(AF_INET6, "ff02::1", &addr.sin6_addr);
	r = bind(fd, &addr, sizeof(addr));
	__ASSERT(r == 0, "bind() failed: %d", r);

	//setup_telnet_ipv6(iface);

	for (size_t i = 0; i < NUM_DEVICES; ++i) {
		if(i == ROTARY) continue;
		LOG_INF("opening device %s", device_labels[i]);
		devices[i] =
			(struct device *)device_get_binding(device_names[i]);
		if (devices[i] == NULL) {
			LOG_ERR("failed to open device %s", device_labels[i]);
			continue;
		}
	}

	/* clear Rotary LEDs */
	for (int i = 0; i<16; i++) {
		led_off(devices[LEDS], i);
	}

	/* setup timer-driven LNK LED event */
	gpio_pin_configure_dt(&led, GPIO_OUTPUT_ACTIVE);
	k_work_init_delayable(&lnk_led_work.dwork, lnk_led_work_handler);
	r = k_work_schedule(&lnk_led_work.dwork, K_MSEC(BLINK_MS));
	__ASSERT(r == 0, "k_work_schedule() failed for LED %u work: %d", 0, r);

	/* setup input-driven button event */
	r = gpio_pin_configure_dt(&button, GPIO_INPUT);
	__ASSERT(r == 0, "gpio_pin_configure_dt() failed: %d", r);
	r = gpio_pin_interrupt_configure_dt(&button, GPIO_INT_EDGE_TO_ACTIVE);
	__ASSERT(r == 0, "gpio_pin_interrupt_configure_dt() failed: %d", r);
	gpio_init_callback(&button_callback_data, button_handler, BIT(button.pin));
	r = gpio_add_callback(button.port, &button_callback_data);
	__ASSERT(r == 0, "gpio_add_callback() failed: %d", r);

	/* setup timer-driven sensor update event */
	k_work_init_delayable(&sensor_work.dwork, sensor_work_handler);
	r = k_work_schedule(&sensor_work.dwork, K_MSEC(UPDATE_MS));
	__ASSERT(r == 0, "k_work_schedule() failed for sensor work: %d", r);

	/* setup input-driven rotary event */
	r = gpio_pin_configure_dt(&enA, GPIO_INPUT);
	r = gpio_pin_interrupt_configure_dt(&enA, GPIO_INT_EDGE_BOTH);
	r = gpio_pin_configure_dt(&enB, GPIO_INPUT);
	rotary_work.position = 0;
	rotary_work.state = gpio_pin_get_dt(&enB);
	gpio_init_callback(&rotary_cb_data, rotary_turned, BIT(enA.pin));
	gpio_add_callback(enA.port, &rotary_cb_data);

	/* listen for position updates */
	k_thread_start(recv_position_thread_id);

	for (;;) {
		k_sleep(K_MSEC(1000));
	}
}
